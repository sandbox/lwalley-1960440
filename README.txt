Feeds XLSX
==========
WARNING: Unsupported experimental module, not intended for general use.

Provides a Feeds (http://drupal.org/project/feeds) FeedsParser plugin to
import data from xlsx files using Feeds.

Requirements
============
- Feeds (http://drupal.org/project/feeds)
- Libraries (http://drupal.org/project/libraries)
- PHP Excel 1.7.8 external library
  (https://github.com/PHPOffice/PHPExcel/archive/PHPExcel_1.7.8.zip)

Installation
============
- Download PHP Excel library 1.7.8 and place in e.g. sites/all/libraries
- Install and enable Feeds XLSX

Developer notes
===============
- Libraries module expects to be able to extract the PHP Excel version
  number from one of the PHP Excel version files, however this is not
  possible because the PHP Excel library does not include the version
  number in any of its files. Consequently Feeds XLSX hard codes the
  library version number in a constant PHPEXCEL_VERSION
  (see feeds_xlsx.module and feeds_xlsx_libraries_info()).
  To change the expected version simply alter the value of the
  PHPEXCEL_VERSION constant, however note that Feeds XLSX has not been
  tested with other versions of PHP Excel.
- An alternative approach to hardcoding the PHP Excel version would be
  to patch PHP Excel library to add the version number to one of the files,
  and then simply add the regular expression to feeds_xlsx_libraries_info()
  instead of the hardcoded version number.
