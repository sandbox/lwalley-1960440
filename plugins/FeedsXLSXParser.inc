<?php

/**
 * @file
 * Contains the FeedsXLSXParser class.
 */

/**
 * Parses a given file as a XLSX file.
 */
class FeedsXLSXParser extends FeedsParser {

  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $source_config = $source->getConfigFor($this);
    if (empty($source_config)) {
      $source_config = $this->getConfig();
    }
    $state = $source->state(FEEDS_PARSE);

    $worksheets = $this->getWorksheets($source_config, $fetcher_result);

    // Parse items.
    $total_items = 0;
    if (empty($source_config['one_item'])) {
      $limit = $source->importer->getLimit();
      $start_item = $state->pointer ? $state->pointer : 1;
      // For easier calculations batch parse one sheet at a time.
      // Figure out which sheet we are currently working on and what the
      // starting rowIndex is.
      $current_worksheet = NULL;
      foreach ($worksheets as $worksheet) {
        $previous_item_total = $total_items;
        $total_items += $worksheet['info']['totalRows'] - intval($worksheet['info']['headerRow']);
        if (is_null($current_worksheet) && $start_item <= $total_items) {
          $current_worksheet = $worksheet;
          $starting_row_index = ($start_item - $previous_item_total) + intval($worksheet['info']['headerRow']);
        }
      }
      $items = $this->parseItems($current_worksheet, $starting_row_index, $limit);
    }
    else {
      // Parse all worksheets into one item.
      $total_items = 1;
      $items = $this->parseAsOneItem($worksheets);
    }

    // Report progress.
    $state->total = $total_items;
    $state->pointer += count($items);
    $state->progress($state->total, $state->pointer);

    return new FeedsParserResult($items, $source->feed_nid);

  }

  /**
   * Batch callback to parse items from the XLSX.
   *
   * @param $source_config
   *   Array of configuration values from import source form.
   * @param $limit
   *   Integer representing the number of items to parse before returning.
   * @return
   *   An array of items keyed by sheet and column names.
   */
  protected function parseItems($worksheet, $starting_row_index = 0, $limit = 0) {
    $items = array();
    foreach ($worksheet['sheet']->getRowIterator() as $row) {
      // Skip rows we've already parsed.
      if ($row->getRowIndex() < $starting_row_index) continue;

      $item = array();
      foreach ($row->getCellIterator() as $cell) {
        $sheet_header_name = "{$worksheet['sheet']->getTitle()}:{$worksheet['info']['headers'][$cell->getColumn()]}";
        $item[$sheet_header_name] = trim($cell->getCalculatedValue());
      }
      $items[] = $item;
      if (count($items) >= $limit) break;
    }

    return $items;
  }

  /**
   * Get worksheets and accompanying info that we want to parse.
   *
   * @return Array
   *   Array of worksheets with accompanying info.
   */
  protected function getWorksheets($source_config, $fetcher_result) {
    $worksheets = array();
    if (($library = libraries_load('PHPExcel')) && !empty($library['loaded'])) {
      // @note FeedsFetcherResult returns the URI (e.g. public://feeds/...) of the
      //       fetched file but PHPExcel returns an empty ZipArchive if the URI is
      //       used, so we need to use the real path.
      $filepath = drupal_realpath($fetcher_result->getFilePath());
      $filetype = PHPExcel_IOFactory::identify($filepath);

      $xlsx_reader = PHPExcel_IOFactory::createReader($filetype);
      $xlsx_reader->setReadDataOnly(true); // Ignore styling and data validation

      $xlsx_info = $xlsx_reader->listWorksheetInfo($filepath);
      $xlsx = $xlsx_reader->load($filepath, TRUE);

      // Get the sheets we are parsing.
      $source_config['sheets_to_parse'] = trim($source_config['sheets_to_parse']);
      if (!empty($source_config['sheets_to_parse'])) {
        $include_sheets = array_map('trim', explode(',', trim($source_config['sheets_to_parse'])));
      }
      foreach ($xlsx->getWorksheetIterator() as $worksheet) {
        if (!isset($include_sheets) || in_array($worksheet->getTitle(), $include_sheets)) {
          $sheet_with_info = array(
            'sheet' => $worksheet,
            'info' => array(),
          );
          // Get accompanying info for worksheet, match on sheet title (not
          // sure if we can rely on index).
          foreach ($xlsx_info as $worksheet_info) {
            if ($worksheet_info['worksheetName'] == $worksheet->getTitle()) {
              $worksheet_info['startRowIndex'] = 1;
              $sheet_with_info['info'] = $worksheet_info;
            }
          }
          $this->parseHeaders($source_config, $sheet_with_info);
          $worksheets[] = $sheet_with_info;
        }
      }
    }
    return $worksheets;
  }

  /**
   * Parse the entire xlsx file as one item.
   */
  protected function parseAsOneItem($worksheets) {
    // All data is one item.
    $item = array();
    foreach ($worksheets as $worksheet) {
      foreach ($worksheet['sheet']->getRowIterator() as $row) {
        // Skip header row.
        if ($worksheet['info']['headerRow'] && $row->getRowIndex() == 1) continue;
        foreach ($row->getCellIterator() as $cell) {
          $sheet_header_name = "{$worksheet['sheet']->getTitle()}:{$worksheet['info']['headers'][$cell->getColumn()]}";
          $item[$sheet_header_name][] = trim($cell->getCalculatedValue());
        }
      }
    }
    return array($item);
  }

  /**
   * Set header info for sheet by reference.
   */
  protected function parseHeaders($source_config, &$worksheet) {
    foreach ($worksheet['sheet']->getRowIterator() as $row) {
      foreach ($row->getCellIterator() as $cell) {
        if (empty($source_config['no_headers'])) {
          $worksheet['info']['headerRow'] = TRUE;
          $worksheet['info']['headers'][$cell->getColumn()] = trim($cell->getCalculatedValue());
        }
        else {
          $worksheet['info']['headerRow'] = FALSE;
          $worksheet['info']['headers'][$cell->getColumn()] = $cell->getColumn();
        }
      }
      break; // We only need first row of each sheet.
    }
  }

  /**
   * Override parent::getMappingSources().
   */
  public function getMappingSources() {
    return FALSE;
  }

  /**
   * Define default values for parser settings form.
   */
  public function configDefaults() {
    return array(
      'no_headers' => 0,
      'one_item' => 0,
      'sheets_to_parse' => '',
    );
  }

  /**
   * Define defaults for an import instance.
   */
  public function sourceDefaults() {
    return $this->config;
  }

  /**
   * Parser settings form.
   */
  public function configForm(&$form_state) {
    $config = $this->getConfig();
    return $this->sourceForm($config);
  }

  /**
   * Standalone import or Feed node import form.
   */
  public function sourceForm($source_config) {
    if (empty($source_config)) {
      $source_config = $this->getConfig();
    }
    $form = array();
    $form['no_headers'] = array(
      '#type' => 'checkbox',
      '#title' => t('No headers'),
      '#description' => t("Check box if the sheets in the import source XLSX file " .
                          "do not start with a header row. If checked, mapping sources " .
                          "must include the column letter along with the sheetname " .
                          "e.g. My Sheet:A, My Sheet:B etc, otherwise use column names " .
                          "e.g. My Sheet:A Column, My Sheet:Another Column."),
      '#default_value' => $source_config['no_headers'],
    );
    $form['one_item'] = array(
      '#type' => 'checkbox',
      '#title' => t('One item'),
      '#description' => t("Check box if all rows across sheets represent just one item " .
                          "and any sheets with multiple rows represent multi-value fields. " .
                          "Leave unchecked if each row across sheets represents an item."),
      '#default_value' => $source_config['one_item'],
    );
    $form['sheets_to_parse'] = array(
      '#type' => 'textfield',
      '#title' => t('Sheets to parse'),
      '#description' => t('Semi-colon separated list of sheets to parse, case sensitive ' .
                          'e.g My sheet; Another Sheet. Leave blank to parse all sheets.'),
      '#default_value' => $source_config['sheets_to_parse'],
    );
    return $form;
  }

}
